#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"

using namespace llvm;

// Constants

enum Token {
    tok_eof = -1,

    tok_def = -2,
    tok_extern = -3,

    tok_identifier = -4,
    tok_number = -5
};

static std::string str_identifier;
static double num_val;
static int curr_tok;
static std::map<char, int> binop_precedence = {
    {'<', 10},
    {'+', 20},
    {'-', 20},
    {'*', 40},
};
constexpr char left_paren = '(', right_paren = ')', comma = ',',
               semicolon = ';', space = ' ', dot = '.', hashmark = '#';
const std::string anonymous_expression = "__anon_expr";

// Lexer

static int gettok() {
    static int last_char = space;

    while (isspace(last_char)) {
        last_char = getchar();
    }

    if (isalpha(last_char)) {
        str_identifier = last_char;
        while (isalnum(last_char = getchar()))
            str_identifier += last_char;

        if (str_identifier == "def")
            return tok_def;
        if (str_identifier == "extern")
            return tok_extern;
        return tok_identifier;
    }

    if (isdigit(last_char) || last_char == dot) {
        std::string num_str;
        bool was_dot = false;
        do {
            was_dot = was_dot || last_char == dot;
            num_str += last_char;
            last_char = getchar();
        } while (isdigit(last_char) || (last_char == dot && !was_dot));

        num_val = strtod(num_str.c_str(), 0);
        return tok_number;
    }

    if (last_char == hashmark) {
        do {
            last_char = getchar();
        } while (last_char != EOF && last_char != '\n' && last_char != '\r');

        if (last_char != EOF)
            return gettok();  // Nice recursive callchain in case there are many
                              // comment lines one after the other
    }

    if (last_char == EOF)
        return tok_eof;

    // Just return the character as-is. I'm not entirely sure when this happens
    // It happens when we consume characters like '(', ')' or ','
    int this_char = last_char;
    last_char = getchar();
    return this_char;
}

// AST

class ExprAST {
   public:
    virtual ~ExprAST() {}
    virtual Value *codegen() = 0;
};

class NumberExprAST : public ExprAST {
    double val_;

   public:
    NumberExprAST(double val) : val_(val) {}

    Value *codegen() override;
};

class VariableExprAST : public ExprAST {
    std::string name_;

   public:
    VariableExprAST(const std::string &name) : name_(name) {}

    Value *codegen() override;
};

class BinaryExprAST : public ExprAST {
    char op_;  // NOTE: op could be an enum
    std::unique_ptr<ExprAST> lhs_, rhs_;

   public:
    BinaryExprAST(char op, std::unique_ptr<ExprAST> lhs,
                  std::unique_ptr<ExprAST> rhs)
        : op_(op), lhs_(std::move(lhs)), rhs_(std::move(rhs)) {}

    Value *codegen() override;
};

class CallExprAST : public ExprAST {
    std::string callee_;
    std::vector<std::unique_ptr<ExprAST>> args_;

   public:
    CallExprAST(const std::string &callee,
                std::vector<std::unique_ptr<ExprAST>> args)
        : callee_(callee), args_(std::move(args)) {}

    Value *codegen() override;
};

class PrototypeAST {
    std::string name_;
    std::vector<std::string> args_;

   public:
    PrototypeAST(const std::string &name, std::vector<std::string> args)
        : name_(name), args_(std::move(args)) {}

    const std::string &name() { return name_; }

    const std::vector<std::string> &args() { return args_; }

    Function *codegen();
};

class FunctionAST {
    std::unique_ptr<PrototypeAST> proto_;
    std::unique_ptr<ExprAST> body_;

   public:
    FunctionAST(std::unique_ptr<PrototypeAST> proto,
                std::unique_ptr<ExprAST> body)
        : proto_(std::move(proto)), body_(std::move(body)) {}

    Function *codegen();
};

// Parser

static int get_next_token() { return curr_tok = gettok(); }

static int get_tok_precedence() {
    if (!isascii(curr_tok))
        return -1;

    int tok_prec = binop_precedence[curr_tok];
    if (tok_prec <= 0)
        return -1;

    return tok_prec;
}

std::unique_ptr<ExprAST> log_error(const char *message) {
    fprintf(stderr, "Error: %s\n", message);
    return nullptr;
}

std::unique_ptr<PrototypeAST> log_error_proto(const char *message) {
    log_error(message);
    return nullptr;
}

static std::unique_ptr<ExprAST> parse_expression();

static std::unique_ptr<ExprAST> parse_number_expr() {
    auto ret = std::make_unique<NumberExprAST>(num_val);
    get_next_token();  // consume the number?
    return std::move(ret);
}

static std::unique_ptr<ExprAST> parse_paren_expr() {
    get_next_token();  // eat '('
    auto value = parse_expression();
    if (!value)
        return nullptr;

    if (curr_tok != right_paren)
        return log_error("expected ')'");
    get_next_token();  // eat ')'
    return value;
}

static std::unique_ptr<ExprAST> parse_identifier_expr() {
    std::string identifier = str_identifier;

    get_next_token();  // eat the identifier;

    if (curr_tok != left_paren)
        return std::make_unique<VariableExprAST>(identifier);

    get_next_token();  // eat the '('
    std::vector<std::unique_ptr<ExprAST>> args;
    if (curr_tok != right_paren) {
        while (true) {
            if (auto arg = parse_expression())
                args.push_back(std::move(arg));
            else
                return nullptr;

            if (curr_tok == right_paren)
                break;

            if (curr_tok != comma)
                return log_error("Expected ')' or ',' in the argument list");
            get_next_token();
        }
    }

    get_next_token();  // eat the ')' again

    return std::make_unique<CallExprAST>(identifier, std::move(args));
}

static std::unique_ptr<ExprAST> parse_primary() {
    switch (curr_tok) {
        default:
            return log_error("Unknown token when expecting expression:");
        case tok_identifier:
            return parse_identifier_expr();
        case tok_number:
            return parse_number_expr();
        case left_paren:
            return parse_paren_expr();
    }
}

static std::unique_ptr<ExprAST> parse_bin_op_rhs(int expression_precedence,
                                                 std::unique_ptr<ExprAST> lhs) {
    while (true) {
        auto token_precedence = get_tok_precedence();

        if (token_precedence < expression_precedence)
            return lhs;

        auto bin_op = curr_tok;
        get_next_token();  // eat the bin_op

        auto rhs = parse_primary();
        if (!rhs)
            return nullptr;

        auto next_precedence = get_tok_precedence();
        if (token_precedence < next_precedence) {
            rhs = parse_bin_op_rhs(token_precedence + 1, std::move(rhs));
            if (!rhs)
                return nullptr;
        }

        lhs =
            std::make_unique<BinaryExprAST>(bin_op, std::move(lhs), std::move(rhs));
    }
}

static std::unique_ptr<ExprAST> parse_expression() {
    auto lhs = parse_primary();
    if (!lhs)
        return nullptr;

    return parse_bin_op_rhs(0, std::move(lhs));
}

static std::unique_ptr<PrototypeAST> parse_prototype() {
    if (curr_tok != tok_identifier)
        return log_error_proto("Expected function name in prototype");

    std::string function_name = str_identifier;
    // std::cerr << "function_name: " << function_name << std::endl;
    get_next_token();

    if (curr_tok != left_paren)
        return log_error_proto("Expected '(' in prototype");

    std::vector<std::string> arg_names;
    while (get_next_token() == tok_identifier)
        arg_names.push_back(str_identifier);

    if (curr_tok != right_paren)
        return log_error_proto("Expected ')' in prototype");

    get_next_token();  // eat ')'

    // std::cerr << "arg_names:";
    // for(const auto el : arg_names) std::cerr << ' ' << el;
    // std::cerr << std::endl;
    return std::make_unique<PrototypeAST>(function_name, std::move(arg_names));
}

static std::unique_ptr<FunctionAST> parse_definition() {
    get_next_token();  // eat def
    auto proto = parse_prototype();
    if (!proto)
        return nullptr;

    if (auto expression = parse_expression())
        return std::make_unique<FunctionAST>(std::move(proto),
                                             std::move(expression));
    return nullptr;
}

static std::unique_ptr<FunctionAST> parse_top_level_expression() {
    if (auto expression = parse_expression()) {
        auto proto = std::make_unique<PrototypeAST>(anonymous_expression,
                                                    std::vector<std::string>());
        return std::make_unique<FunctionAST>(std::move(proto),
                                             std::move(expression));
    }
    return nullptr;
}

static std::unique_ptr<PrototypeAST> parse_extern() {
    get_next_token();  // eat extern
    return parse_prototype();
}

// Code Generator

static std::unique_ptr<LLVMContext> llvm_context;
static std::unique_ptr<Module> llvm_module;
static std::unique_ptr<IRBuilder<>> builder;
static std::map<std::string, Value *> named_values;

Value *log_error_v(const char *str) {
    log_error(str);
    return nullptr;
}

Value *NumberExprAST::codegen() {
    return ConstantFP::get(*llvm_context, APFloat(val_));
}

Value *VariableExprAST::codegen() {
    Value *v = named_values[name_];
    if (!v) {
        return log_error_v("Unknown variable name");
    }
    return v;
}

Value *BinaryExprAST::codegen() {
    Value *lhs = lhs_->codegen(), *rhs = rhs_->codegen();
    if (!lhs || !rhs) {
        return nullptr;
    }

    switch (op_) {
        case '+':
            return builder->CreateFAdd(lhs, rhs, "addtmp");
        case '-':
            return builder->CreateFSub(lhs, rhs, "subtmp");
        case '*':
            return builder->CreateFMul(lhs, rhs, "multmp");
        case '<':
            // The value of lhs is a bool 1/true if lhs < rhs and 0/false if not
            lhs = builder->CreateFCmpULT(lhs, rhs, "cmptmp");
            // Convert 0 -> 0.0 and 1->1.0
            return builder->CreateUIToFP(lhs, Type::getDoubleTy(*llvm_context), "booltmp");
        default:
            return log_error_v("Invalid binary operator");
    }
}

Value *CallExprAST::codegen() {
    Function *callee = llvm_module->getFunction(callee_);
    if (!callee) {
        return log_error_v("Unknown function referenced");
    }

    if (callee->arg_size() != args_.size()) {
        return log_error_v("Incorrect number of arguments passed");
    }

    std::vector<Value *> args;
    for (unsigned i = 0; i < args_.size(); i++) {
        args.push_back(args_[i]->codegen());
        if (!args.back()) {
            return nullptr;
        }
    }

    return builder->CreateCall(callee, args, "calltmp");
}

Function *PrototypeAST::codegen() {
    std::vector<Type *> arg_types(args_.size(), Type::getDoubleTy(*llvm_context));
    FunctionType *function_type = FunctionType::get(Type::getDoubleTy(*llvm_context), arg_types, false);
    Function *function = Function::Create(function_type, Function::ExternalLinkage, name_, llvm_module.get());

    unsigned i = 0;
    for (auto &arg : function->args()) {
        arg.setName(args_[i++]);
    }

    return function;
}

Function *FunctionAST::codegen() {
    Function *function = llvm_module->getFunction(proto_->name());

    if (!function) {
        function = proto_->codegen();
        if (!function) {
            return nullptr;
        }
    }

    BasicBlock *bb = BasicBlock::Create(*llvm_context, "entry", function);
    builder->SetInsertPoint(bb);

    named_values.clear();
    for (auto &arg : function->args()) {
        named_values[std::string(arg.getName())] = &arg;
    }

    if (Value *ret_val = body_->codegen()) {
        builder->CreateRet(ret_val);
        verifyFunction(*function);
        return function;
    }

    function->eraseFromParent();
    return nullptr;
}

// Top level parsing

static void initialize_module() {
    llvm_context = std::make_unique<LLVMContext>();
    llvm_module = std::make_unique<Module>("my module", *llvm_context);
    builder = std::make_unique<IRBuilder<>>(*llvm_context);
}

static void handle_definition() {
    if (auto function_ast = parse_definition()) {
        if (auto *function_ir = function_ast->codegen()) {
            fprintf(stderr, "Parsed a function definition\n");
            function_ir->print(errs());
            fprintf(stderr, "\n");
        }
    } else {
        get_next_token();  // Skip token for error recovery
    }
}

static void handle_extern() {
    if (auto proto_ast = parse_extern()) {
        if (auto *function_ir = proto_ast->codegen()) {
            fprintf(stderr, "Parsed extern\n");
            function_ir->print(errs());
            fprintf(stderr, "\n");
        }
    } else {
        get_next_token();  // Skip token for error recovery
    }
}

static void handle_top_level_expression() {
    if (auto function_ast = parse_top_level_expression()) {
        if (auto *function_ir = function_ast->codegen()) {
            fprintf(stderr, "Parsed a top level expression\n");
            function_ir->print(errs());
            fprintf(stderr, "\n");
            function_ir->eraseFromParent();
        }
    } else {
        get_next_token();  // Skip token for error recovery
    }
}

static void main_loop() {
    while (true) {
        switch (curr_tok) {
            case tok_eof:
                return;
            case semicolon:
                get_next_token();
                break;
            case tok_def:
                handle_definition();
                break;
            case tok_extern:
                handle_extern();
                break;
            default:
                handle_top_level_expression();
                break;
        }
        fprintf(stderr, "ready> ");
    }
}

// Main driver code

int main() {
    fprintf(stderr, "ready> ");
    get_next_token();  // start the process

    initialize_module();

    main_loop();

    return 0;
}