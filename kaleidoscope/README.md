# Tutorial to the Kaleidoscope compiler

## How to compile it

`clang++ -g -O3 -o compiler compiler.cc $(llvm-config --cxxflags)`

## Examples 

Examples are the `.kal` files in the examples folder