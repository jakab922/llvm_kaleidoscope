with import <nixpkgs> {};

stdenv.mkDerivation {
    name = "kaleidoscope_compiler";
    src = ./.;

    buildInputs = [ clang clangStdenv llvm ];

    buildPhase = "clang++ $(llvm-config --cxxflags --ldflags) -std=c++17 $(llvm-config --libs all --system-libs) -o compiler compiler.cc";

    shellHook = ''
      export LOCALE_ARCHIVE=/usr/lib/locale/locale-archive
    '';

    installPhase = ''
      mkdir -p $out/bin
      cp compiler $out/bin/
    '';
}